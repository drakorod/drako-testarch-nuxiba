using UsuarioApi.Models;
using System.Collections.Generic;
using System.Linq;

namespace UsuarioApi.Services {
    public class UsuarioServices {

        private readonly UsuarioApiDbContext _context;
        private readonly List<Usuario> Usuarios;

        public UsuarioServices(UsuarioApiDbContext context){
            _context = context;
        }

        public List<Usuario> GetUsuarios()
        {
            return _context.Usuario.ToList();
        }
    }
}