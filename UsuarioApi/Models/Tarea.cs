using System.ComponentModel.DataAnnotations;

namespace UsuarioApi.Models {

    public class Tarea 
    {
        [Key]
        public int IdTarea {get; set;}
        public string NombreTarea {get; set;}
        public string Descripcion {get; set;}
        public string FechaCreacion {get; set;}
        
    }
}