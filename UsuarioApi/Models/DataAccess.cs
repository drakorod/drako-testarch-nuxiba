using Microsoft.EntityFrameworkCore;

namespace UsuarioApi.Models 
{
    public class UsuarioApiDbContext : DbContext 
    {
        public UsuarioApiDbContext (DbContextOptions<UsuarioApiDbContext> data):base(data) {}

        public DbSet<Usuario> Usuario{get; set;}

        public DbSet<Tarea> Tarea{get; set;}
        public DbSet<EstadoTarea> EstadoTarea{get; set;}
    }
}