using System.ComponentModel.DataAnnotations;

namespace UsuarioApi.Models {

    public class Usuario 
    {
        [Key]     
        public int IdUsuario {get; set;}
        public string NombreUsuario {get; set;}
        public string Contrasenia {get; set;}
        public string Nombre {get; set;}
        
    }
}