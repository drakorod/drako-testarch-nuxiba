using System.ComponentModel.DataAnnotations;

namespace UsuarioApi.Models
{
    public class AuthenticateRequest
    {
        [Required]
        public string Usuario { get; set; }

        [Required]
        public string Password { get; set; }
    }
}