using System.ComponentModel.DataAnnotations;

namespace UsuarioApi.Models
{
    public class AuthenticateResponse
    {
        public int IdUsuario;
        public string NombreUsuario { get; set; }
        public string Nombre { get; set; }
        public string Token { get; set; }
        public AuthenticateResponse(Usuario user, string token)
        {
           if (token != "") {
            IdUsuario = user.IdUsuario;
            NombreUsuario = user.NombreUsuario;
            Nombre = user.Nombre;
            Token = token;
           } else {
               NombreUsuario = user.NombreUsuario;
           }
        }
    }
}