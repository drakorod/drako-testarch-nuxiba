using System.ComponentModel.DataAnnotations;

namespace UsuarioApi.Models {

    public class EstadoTarea 
    {
        [Key]
        public int IdEstadoTarea {get; set;}
        public string Nombre {get; set;}
        public string Descripcion {get; set;}        
    }
}