using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using UsuarioApi.Models;
using UsuarioApi.Services;
using Microsoft.Extensions.Logging;

namespace UsuarioApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TareaController : ControllerBase {
        private readonly UsuarioApiDbContext _context;

        private readonly ILogger _logger;
        public TareaController(UsuarioApiDbContext context, ILogger<LoginController> logger) {
            _context = context;
            _logger = logger;
        }

        [HttpPost]
        public ActionResult<Tarea> Create([FromBody] Tarea tarea){
            tarea.IdTarea = _context.Tarea.Any() ? _context.Tarea.Max(t => t.IdTarea) + 1 : 1;
             this._context.Tarea.Add(tarea);
            this._context.SaveChanges();
            return Created($"tarea/{tarea.IdTarea}", tarea);
        }

        //Search by ID
        [HttpGet("{id:int}")]
        public IActionResult GetTareaById(int id){
            var tarea = this._context.Tarea.SingleOrDefault(ct=> ct.IdTarea ==id);
                if(tarea != null){
                    return Ok(tarea);
                }else{
                    return NotFound();
                }
            }
    }

}