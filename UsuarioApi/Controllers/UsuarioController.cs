using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using UsuarioApi.Models;
using UsuarioApi.Services;
using System.Text.Json;
using Microsoft.AspNetCore.Http;

namespace UsuarioApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsuarioController : ControllerBase {
        
        private readonly UsuarioApiDbContext _context;

        public UsuarioController(UsuarioApiDbContext context){
            _context = context;
        }

        [HttpGet]
        public ActionResult<List<Usuario>> GetAll() {
            return _context.Usuario.ToList();
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Usuario> Create([FromBody] Usuario usuario){
        
            usuario.IdUsuario = _context.Usuario.Any() ? _context.Usuario.Max(u => u.IdUsuario) + 1 : 1;
            this._context.Usuario.Add(usuario);
            this._context.SaveChanges();
            return Created($"usuario/{usuario.IdUsuario}", usuario);
        }

        //Search by ID
        [HttpGet("{id:int}")]
        public IActionResult GetUsuarioById(int id){
            var usuario = this._context.Usuario.SingleOrDefault(ct=> ct.IdUsuario ==id);
                if(usuario != null){
                    return Ok(usuario);
                }else{
                    return NotFound();
                }
            }
    }
}