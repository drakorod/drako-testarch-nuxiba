using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using UsuarioApi.Models;
using UsuarioApi.Services;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using UsuarioApi.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System;
using Microsoft.Extensions.Logging;

namespace UsuarioApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LoginController : ControllerBase {

        private readonly UsuarioApiDbContext _context;
        private readonly AppSettings _appSettings;

        private readonly ILogger _logger;

        public LoginController(UsuarioApiDbContext context, ILogger<LoginController> logger) {
            _context = context;
            _logger = logger;
        }

        [HttpPost("auth")]
        public ActionResult<AuthenticateResponse> Authenticate([FromBody] AuthenticateRequest usuario){
            var user = _context.Usuario.SingleOrDefault(u => u.NombreUsuario == usuario.Usuario && u.Contrasenia == usuario.Password);

            if (user == null) {
                user = new Usuario();
                user.NombreUsuario = usuario.Usuario;
                //return new AuthenticateResponse(user, "");
                return BadRequest(new {});
            } else{
                _logger.LogInformation($"IdUsuario --> {user.IdUsuario}");
                var token = generateJwtToken(user);
                return new AuthenticateResponse(user, token);
            }
        }

        private string generateJwtToken(Usuario usuario)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("kCET7MhX68WBXWZEVV7JYZBleiAIIMSn");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("idUsuario", usuario.IdUsuario.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }

}