import React, { useState } from 'react';
import './Register.css'

async function saveUser(user) {
    return fetch('api/usuario', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(user)
    }).then(data => data.json())
  }

export default function Register() {
    const [NombreUsuario, setUserName] = useState()
    const [Password, setPassword] = useState()
    const [Nombre, setNombre] = useState()

    const handleSubmit = async e => {
        e.preventDefault()
        saveUser({Usuario,Password})
      }

    return (
        <div className="login-wrapper">
        <h1>Please Register</h1>
        <form onSubmit={handleSubmit}>
          <label>
            <p>NombreUsuario</p>
            <input type="text" onChange={e => setUserName(e.target.value)} />
          </label>
          <label>
            <p>Password</p>
            <input type="password" onChange={e => setPassword(e.target.value)} />
          </label>
          <label>
            <p>Nombre</p>
            <input type="text" onChange={e => setNombre(e.target.value)} />
          </label>
          <div>
            <button type="submit">Submit</button>
          </div>
        </form>
      </div>
        
    );
}