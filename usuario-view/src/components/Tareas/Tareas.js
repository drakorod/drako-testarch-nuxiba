import React, { useState } from 'react';

async function addUser(UserData) {
    return fetch('api/usuario/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(UserData)
    }).then(data => data.json())
  }

export default function Tareas() {
    const [Usuario, setUserName] = useState()
    const [Password, setPassword] = useState()

    const handleSubmit = async e => {
        e.preventDefault()
        //const json = await addUser({Usuario,Password})
        await addUser({Usuario,Password})
        
    }

    return (
        <div className="wrapper">
            <h3>Tareas</h3>
            <form onSubmit={handleSubmit}>
                <label>
                    <p>Usuario</p>
                    <input type="text" onChange={e => setUserName(e.target.value)} />
                </label>
                <label>
                    <p>Password</p>
                    <input type="password" onChange={e => setPassword(e.target.value)} />
                </label>
                <div>
                    <button type="submit">Submit</button>
                </div>
            </form>
        </div>
    );
}   