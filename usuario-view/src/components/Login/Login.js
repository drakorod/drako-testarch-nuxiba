import React, { useState } from 'react'
import PropTypes from 'prop-types'
import './Login.css'


async function loginUser(credentials) {
  return fetch('api/login/auth', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  }).then(data => data.json())
}

export default function Login({ setToken }) {
    const [Usuario, setUserName] = useState()
    const [Password, setPassword] = useState()

    const handleSubmit = async e => {
      e.preventDefault()
      const json = await loginUser({Usuario,Password})
      setToken(json)
    }

    return (
        <div className="login-wrapper">
        <h1>Please Log In</h1>
        <form onSubmit={handleSubmit}>
          <label>
            <p>Usuario</p>
            <input type="text" onChange={e => setUserName(e.target.value)} />
          </label>
          <label>
            <p>Password</p>
            <input type="password" onChange={e => setPassword(e.target.value)} />
          </label>
          <div>
            <button type="button">Register</button>
            <button type="submit">Submit</button>
          </div>
        </form>
      </div>
    );

}

Login.prototype = {
  setToken: PropTypes.func.isRequired
}