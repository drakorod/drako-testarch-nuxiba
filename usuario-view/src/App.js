import React from 'react';
import './App.css';
//import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Tareas from './components/Tareas/Tareas';
import Login from './components/Login/Login';
//import Preferences from './components/Preferences/Preferences';
import useToken from './components/App/useToken';

function App() {
  const {token, setToken} = useToken()

  if (!token) {
    return <Login setToken={setToken}/>
  }

  return (
    <div className="wrapper">
      <h2>App</h2>
      <Tareas />
    </div>
  );
} 

export default App;
