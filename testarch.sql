---------------------------
--SQL Server
---------------------------


CREATE TABLE testarch.dbo.Usuario (
	IdUsuario int NOT NULL,
	NombreUsuario varchar(255) NOT NULL,
	Contrasenia varchar(255) NOT NULL,
	Nombre varchar(255) NOT NULL,
	CONSTRAINT Usuario_PK PRIMARY KEY (IdUsuario)
);
EXEC testarch.sys.sp_addextendedproperty 'MS_Description', N'Contiene los usuarios', 'schema', N'dbo', 'table', N'Usuario';

CREATE TABLE testarch.dbo.EstadoTarea (
	IdEstadoTarea int NOT NULL,
	Nombre varchar(255) NULL,
	Descripcion varchar(500) NULL,
	CONSTRAINT EstadoTarea_PK PRIMARY KEY (IdEstadoTarea)
);

CREATE TABLE testarch.dbo.Tarea (
	IdTarea int NOT NULL,
	NombreTarea varchar(255) NULL,
	Descripcion varchar(500) NULL,
	FechaCreacion timestamp NULL,
	IdUsuario int NULL,
	IdEstadoTarea int NULL,
	CONSTRAINT Tarea_PK PRIMARY KEY (IdTarea),
	CONSTRAINT Tarea_FK FOREIGN KEY (IdUsuario) REFERENCES testarch.dbo.Usuario(IdUsuario),
	CONSTRAINT Tarea_FK_1 FOREIGN KEY (IdEstadoTarea) REFERENCES testarch.dbo.EstadoTarea(IdEstadoTarea)
);


INSERT INTO Usuario VALUES (1, 'drakorod', '123456', 'Alfredo');

